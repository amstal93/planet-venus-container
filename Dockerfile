FROM docker.io/library/python:2.7.18-slim-buster

# xsltproc - generate Planet Venus RSS feeds correctly
# make - support build with Makefile
# locales, locales-all - support for regional locales, e.g. date formats
RUN apt-get update
RUN apt-get install -y xsltproc make locales locales-all

# Planet Venus installation directory
ENV VENUS_DIR="/opt/planet-venus"

# https://www.intertwingly.net/code/venus/docs/installation.html
# https://www.intertwingly.net/code/venus/
# https://github.com/rubys/venus
RUN mkdir -p "${VENUS_DIR}"
ADD https://gitlab.com/OpenAlt/planet-venus-container/-/archive/venus-source/planet-venus-container-venus-source.tar.gz /tmp
RUN tar -xzf /tmp/planet-venus-container-venus-source.tar.gz --strip-components=1 -C "${VENUS_DIR}" --no-same-owner
RUN "${VENUS_DIR}/runtests.py"

ENV PATH="${VENUS_DIR}:${PATH}"

WORKDIR "${VENUS_DIR}"

ENTRYPOINT ["planet.py"]
